const yargs = require("yargs");
const axios = require("axios");

const argv = yargs
    .options({
        address:  {
            alias: "a",
            demand: true,
            describe: "Address",
            string: true
        }
    })
    .help()
    .alias("help" , "h")
    .argv;

var encodedAddress = encodeURIComponent(argv.address);
var geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`;

axios.get(geocodeUrl).then((response) => {
    if(response.data.status === "ZERO_RESULTS"){
        throw new Error("unable to find that address")
    }
    
    var lat = response.data.results[0].geometry.location.lat;
    var lng = response.data.results[0].geometry.location.lng;
    var weatherUrl = `https://api.forecast.io/forecast/ccf79f22a96a03c7214ecdb10619361f/${lat},${lng}`;

    console.log(response.data.results[0].formatted_address)
    return axios.get(weatherUrl);
}).then((response) => {
    var temperature = response.data.currently.temperature;
    var  lowtemp = response.data.daily.data[0].temperatureLow;
    var hightemp =  response.data.daily.data[0].temperatureHigh;
    console.log(`Its currently ${temperature}.`);
    console.log(`TemperatureLow: ${lowtemp}`);
    console.log(`TemperatureHigh: ${hightemp}`)
}).catch((e) => {
    if(e.code === "ENOTFOUND"){
        console.log("unable to connect to API server")
    }else{
        console.log(e.message)
    }
})