const yargs = require("yargs");

const geocode = require("./geocode/geocode");
const weather = require("./weather/weather")

const argv = yargs
    .options({
        address:  {
            alias: "a",
            demand: true,
            describe: "Address",
            string: true
        }
    })
    .help()
    .alias("help" , "h")
    .argv;

geocode.geocodeAddress(argv.address, (errorMessage, results) => {
    if(errorMessage){
        console.log(errorMessage);
    }else{
        console.log(results.Address);
        weather.getWeather(results.Latitude, results.Longitude, (errorMessage, weatherResults) => {
    if(errorMessage){
        console.log(errorMessage);
    }else {
        var x = 5/9*(weatherResults.temperature -32);
        var y = x.toFixed(2);
        var x1 = 5/9*(weatherResults.lowtemp -32);
        var y1 = x1.toFixed(2);
        var x2 = 5/9*(weatherResults.hightemp -32);
        var y2 = x2.toFixed(2);
        console.log(`its currently ${y}`);
        console.log(`temperatureLow: ${y1}`);
        console.log(`temperatureHigh: ${y2}`)
    }
    });
   }
})



