const request = require("request");

var geocodeAddress = (address, callback) => {
var encodedAddress = encodeURIComponent(address);

request({
    url:`https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`,
    json: true
},(error, response, body) => {
    if(error){
        callback("unable to connact to Google srevers")
    }
    else if(body.status === "ZERO_RESULTS"){
        callback("unable to find that address")
    }else if(body.status === "OK"){
        callback(undefined, {
            Address: body.results[0].formatted_address,
            Latitude: body.results[0].geometry.location.lat,
            Longitude: body.results[0].geometry.location.lng
    });
}
else {
        callback(undefined, {
            Address: "Yerevan, Armenia",
            Latitude: 40.1791857,
            Longitude: 44.4991029
    })
   }
 });
}

module.exports.geocodeAddress = geocodeAddress;