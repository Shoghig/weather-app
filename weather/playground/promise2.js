const request = require("request");

var geocodeAddress = (address) => {
    return new Promise((resolve, reject) => {
    var encodedAddress = encodeURIComponent(address);

request({
    url:`https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`,
    json: true
},(error, response, body) => {
    if(error){
        reject("unable to connact to Google srevers")
    }
    else if(body.status === "ZERO_RESULTS"){
        reject("unable to find that address")
    }else if(body.status === "OK"){
        resolve({
            Address: body.results[0].formatted_address,
            Latitude: body.results[0].geometry.location.lat,
            Longitude: body.results[0].geometry.location.lng
    });
}
else {
        resolve({
            Address: "Yerevan, Armenia",
            Latitude: 40.1791857,
            Longitude: 44.4991029
    })
   }
 });
})
}

geocodeAddress("").then((location) => {
    console.log(JSON.stringify(location, undefined, 2))
}, (errorMessage) =>{
    console.log(errorMessage)
});