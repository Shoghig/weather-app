const request = require("request");

var getWeather = (lat, lng ,callback) => {
request({
    url: `https://api.darksky.net/forecast/ccf79f22a96a03c7214ecdb10619361f/${lat},${lng}`,
    json: true
}, (error, response, body) => {
    if(error){
        callback("Unable to connect to forecast server");
    }else if(response.statusCode === 400){
       callback("Unable to fetch weather");
    }else if(response.statusCode === 200){
        callback(undefined, {
            temperature: body.currently.temperature,
            lowtemp: body.daily.data[0].temperatureLow,
            hightemp: body.daily.data[0].temperatureHigh
        })
    }
  });
}

module.exports.getWeather = getWeather;